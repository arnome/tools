from setuptools import setup

setup(name='manage_sci_project',
      version='0.1',
      description='How to manage scientific project tree',
      url='https://framagit.org/arnome/tools',
      author='Arnaud Mounier',
      author_email='arnaud.mounier@inra',
      license='CECILL',
      packages=['manage_sci_project'],
      zip_safe=False,
      include_package_data=True)
