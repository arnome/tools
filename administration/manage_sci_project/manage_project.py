#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pathlib
import yaml
from datetime import datetime

def rename_nested_key(d, old_key, new_key):
    """rename_nested_key remane a key in a nested dict"""
    if isinstance(d, dict):
        if old_key in d.keys():
            try:
                d[new_key] = d.pop(old_key)
            except KeyError as ex:
                raise Exception("This key ({}) is not present is the structure.{}".format(old_key,ex))
        else:

            for element in d:
                if isinstance(d[element], dict):
                    rename_nested_key(d[element], old_key, new_key)
                elif isinstance(d[element], list):
                    rename_nested_key(d[element], old_key, new_key)
    elif isinstance(d, list):
        for element in d:
            if isinstance(element, dict):
                rename_nested_key(element, old_key, new_key)

def dict_to_dir(data, path=str()):
    """dict_to_dir expects data to be a dictionary with one top-level key."""

    dest = os.path.join(os.getcwd(), path)

    if isinstance(data, dict):
        for k, v in data.items():
            os.makedirs(os.path.join(dest, k))
            dict_to_dir(v, os.path.join(path, str(k)))

    elif isinstance(data, list):
        for i in data:
            if isinstance(i, dict):
                dict_to_dir(i, path)
            else:
                with open(os.path.join(dest, i), "a"):
                    os.utime(os.path.join(dest, i), None)

    if isinstance(data, dict):
        return list(data.keys())[0]


class Experiment:
    """Class experiment store and write the experiment part"""
    def __init__(self, path, name=
                 None, date=None, params=None):
        if name:
            self.name = name
        else:
            if date:
                if params:
                    self.name = date + "_" + params
                else:
                    self.name = date
            else:
                if params:
                    self.name = self._set_default_date() + "_" + params
                else:
                    self.name = self._set_default_date()
        self.path = path

    def _set_default_date(self):
        now = datetime.now()
        return now.strftime('%Y-%m-%d')

    def set_tree(self):
        """set_tree(
        ) write to the path the tree of the project (wide or expl)"""
        descr = pathlib.Path(__file__).parent / "conf/experience.yml"
        try:
            with open(descr) as fh:
                try:
                    tree = yaml.load(fh)
                    rename_nested_key(tree, "EXP_NAME", self.name)
                    dest = dict_to_dir(data=tree, path=self.path)
                    print("Experiment created at {0:s}".format(self.path))
                    self.tree = tree
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)

class Project:
    """Class Project store the project itsel"""

    def __init__(self, name, project_type=None, path=None):
        self.name = name
        if project_type:
            if project_type != "wide-project" or project_type != "expl-project":
                raise Exception("Wrong type of project : must be 'wide-project' or 'expl-project'.")
            else:
                self.project_type = project_type
        else:
            self.project_type = self._set_default_project_type()

        if path:
            if os.path.isdir(path):
                self.path = os.path.abspath(path)
            else:
                raise Exception("{} not a regular folder".format(path))

        else:
            self.path = os.path.abspath(os.getcwd())

    def _set_default_project_type(self):
        """Return the default project type (wide-project)"""
        return "wide-project"


    def set_tree(self):
        """set_tree() write to the path the tree of the project (wide or expl)"""
        if os.path.isdir(os.path.join(self.path,self.name)):
            print("This Project allready exist \n")
            sys.exit(1)

        if self.project_type == "wide-project":
            descr = pathlib.Path(__file__).parent / "conf/wide-project.yml"
            descr_exp = pathlib.Path(__file__).parent / "conf/experience.yml"
        elif self.project_type == "expl-project":
            descr = pathlib.Path(__file__).parent / "conf/expl-project.yml"
        try:
            with open(descr) as fh:
                try:
                    tree = yaml.load(fh)
                    rename_nested_key(tree, "PROJECT_NAME", self.name)
                    project_name_rp = self.name + ".Rp"
                    rename_nested_key(tree, "PROJECT_NAME.Rp", project_name_rp)

                    dest = dict_to_dir(data=tree, path=self.path)
                    print("{0:s} created at {1:s}".format(self.project_type, self.path))
                    self.tree = tree
                    exp = Experiment(os.path.join(self.name,"exp"))
                    exp.set_tree()
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)

    def show_current_tree(self):
        return 1

    def show_available_trees(self, project_type):
        return 1

