#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import errno
import subprocess
from shlex import split

def silent_remove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

def run_shell_command_pipe(shell_command, run_cmd, print_cmd):
    """
    run shell command with pipe in shell style and return proper output
    """
    if print_cmd:
        print(shell_command)
    if run_cmd:
        process = subprocess.Popen(shell_command, shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = process.communicate()
        if out:
            return(out.decode("utf-8"))
        if err:
            return(err.decode("utf-8"))

def run_shell_command(shell_command, run_cmd, print_cmd):
    if print_cmd:
        print(shell_command)
    if run_cmd:
        shell_command = split(shell_command)
        process = subprocess.Popen(shell_command, shell = False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (out, err) = process.communicate()
        if out:
            return(out.decode("utf-8"))
        if err:
            return(err.decode("utf-8"))



